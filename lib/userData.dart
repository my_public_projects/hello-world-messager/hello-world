
//------------------------------------------------------------------------------

class UserData {
  final String userName;
  final String token;
  UserData(this.userName, this.token)
      : assert(userName.isNotEmpty),
        assert(token.isNotEmpty);
}