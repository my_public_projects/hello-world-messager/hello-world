import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';



import 'userData.dart';
//import 'fakeChatsGen.dart';
import 'chatData.dart';
import 'chatWidget.dart';
import 'app_client.dart';

//------------------------------------------------------------------------------
class Messager extends StatefulWidget {


  final UserData _userData;


  Messager(this._userData) : assert(_userData != null);

  @override
  _MessagerState createState() =>
      _MessagerState(_userData);
}

//------------------------------------------------------------------------------
class _MessagerState extends State<Messager> {

  final UserData _userData;
  Map<String, ChatData> _chats = Map<String, ChatData>();
  final _biggerFont = TextStyle(fontSize: 14.0, color: Colors.black);
  Timer _timer ;

  ChatWidget _current_chat;

  _MessagerState(this._userData) : assert(_userData != null)
  {
    // final fakeGen = FakeChatsGen(_userData.userName);
    // _chats = fakeGen.fakeChats(38);
    _getChats();
    _timer = Timer(Duration(seconds: 5), _getUpdates);
  }

  void _getUpdates()
  {
      _timer = Timer(Duration(seconds: 8), _getUpdates);
      APPClient().getUpdates(
          _userData.token,
              (result) {
                  final int count = int.tryParse(result["count"]);
                  if(count <= 0) return;

                  List<dynamic> updates = result["updates"] ;
                  bool hasNewChats = false;
                  for(Map<String, dynamic> upd in updates) {

                      String type = upd["type"];
                      if(type == "newmessage") {
                          String chat_id = upd["chatid"];
                          if(!_chats.containsKey(chat_id)) {
                            hasNewChats = true;
                            continue;
                          }

                          _chats[chat_id].newMessages += 1;

                      }

                  }
                  
                  if(hasNewChats) {
                    _getChats();
                  } else
                    {
                      setState(() {

                      });
                    }
                  

              },
              (message) {
                  print("CAN'T GET UPDATES:" + message);
              }
              );
  }

  void _onListTap(ChatData chat)
  {

      setState(() {
          chat.newMessages = 0;
      });

      APPClient().getChat(
          _userData.token,
          chat.chatId,
              (result) {
                  List<dynamic> rawMessages = result["messages"];

                  List<Message> messages = rawMessages.map((e) {
                    Map<String, dynamic> jmessage = e;

                    Message message = Message(jmessage["data"], jmessage["owner"]);
                    message.time = jmessage["time"];

                    return message;

                  }).toList();

                  chat.messages = messages;

                  _pushChat(chat);

              },
              (message) {
                  print("Error: Can't get chat" + message);
              }
              );
  }

  List<ListTile> _buildChatListItems()
  {
     List<ListTile> listItems = List<ListTile>();
    for(ChatData chat in _chats.values) {
        listItems.add(
            ListTile(
              title: Text(chat.chatName, style: _biggerFont,),
              trailing: Text( (chat.newMessages > 0 ? "+" + chat.newMessages.toString() : "")),
              onTap: (){ _onListTap(chat);},
            )
        );
    }


    return listItems;
  }

  Widget _buildChats()
  {
    return ListView(children: _buildChatListItems(),);
  }

  void _pushChat(ChatData chat)
  {
    Navigator.of(context).push(
        MaterialPageRoute<void>(
            builder: (BuildContext context)
            {
              _current_chat = ChatWidget(chat, _userData);

              return _current_chat;

            }
        )
    );
  }

  void _getChats()
  {
    APPClient().getChatList(
        _userData.token,
            (result) {
          if(!(result["chats"] is List<dynamic> )) {
              print("[GET CAHTS] : List is not List. Lol");
              return;
          }
          final List<dynamic> headersList = result["chats"];
          if(headersList.isEmpty) {
            print("[MESSAGER]: CHAT LIST IS NULL");
            return;
          }

          final chat_datas = headersList.map((e){
            Map<String, dynamic> jchat = e;

            print("${ jchat["name"]} : ${jchat["id"]}");

            ChatData chat = ChatData();
            chat.chatName = jchat["name"];
            chat.chatId = jchat["id"];
            return chat;
          });

          final chatlist = chat_datas.toList();

          setState(() {
            final Map<String, ChatData> temp = Map<String, ChatData>();

            for(ChatData chat in chatlist) {
              if(!_chats.containsKey(chat.chatId)) {
                _chats[chat.chatId] = chat;
              }
            }

          });

        },
            (message) {
          //TODO handle error and unlogin;
          print("[MESSAGER]: GET LIST ERROR!");
        });
  }

  void _onCreateAction(BuildContext context, String name)
  {

    APPClient().createChat(
        _userData.token,
        name,
            (result) {
                Navigator.of(context).pop();
                _getChats();
                },
            (message) {
                showDialog(
                  context: context,
                  builder: (BuildContext context)
                    {
                      return AlertDialog(title: Text("Error: " + message),);
                    }
                );
            }
    );
  }

  void _createChat(BuildContext context)
  {

    final _text_controller = TextEditingController();

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Chat name:"),
            content:
                TextField(
                  controller: _text_controller,
                  decoration: InputDecoration(hintText: "enter chat name"),
                )
              ,
            actions: [RaisedButton(
              child: Text("create"),
              onPressed: () {_onCreateAction(context, _text_controller.text);}
              ,)
            ],
          );
        });


  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Row(
          children : [Text('WTFsager'), Text("Chats:")],
        ),
        //TODO SETTings?
        actions: [
          IconButton(icon: Icon(Icons.add), onPressed: (){_createChat(context);}),
        ],
      ),
      body: _buildChats(),
    );
  }
}//_MessagerState