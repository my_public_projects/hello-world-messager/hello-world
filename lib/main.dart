

import 'package:flutter/material.dart';
import 'package:flutter_app/userData.dart';
import 'loginRegisterPage.dart';

import 'messager.dart';
import 'app_client.dart';




void main() {

  print(APPClient().server() + ":" + APPClient().port().toString());

  runApp(MyApp());
}



//------------------------------------------------------------------------------
class MyApp extends StatefulWidget
{


  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }



}


class _MyAppState extends State<MyApp> {

  bool _isLogined = false;
  UserData _userData;

  void _onLogin(UserData userData)
  {
      setState(() {
        _isLogined = true;
        _userData = userData;
      });
  }

  @override
  Widget build(BuildContext conext)
  {
    return MaterialApp(
      title: "HELLO WORLD Messager",
      theme: ThemeData(
        primaryColor: Colors.black,
      ),
      home: _isLogined ? Messager(_userData) : LoginRegisterPage(_onLogin),
    );
  }
}