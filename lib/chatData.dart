//------------------------------------------------------------------------------
class Message {
  String data = "";
  String time = DateTime.now().toString();
  String owner = "";

  Message(this.data, this.owner)
      : assert(data != null),
        assert(owner != null);

}//Message

//------------------------------------------------------------------------------
class ChatData {
  String chatName = "Non Name";
  String chatId = "";
  List<Message> messages = <Message>[];
  int newMessages = 0;
}//ChatData