import 'package:flutter/material.dart';
import 'package:flutter_app/userData.dart';
import 'app_client.dart';

void showMessage(String message, BuildContext context)
{
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(title: Text(message));
      });
}

class LoginRegisterPage extends StatelessWidget {


  final void Function(UserData userData) onLogin;

  LoginRegisterPage(this.onLogin);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Builder(builder: (BuildContext context) {
        final TabController tabController = DefaultTabController.of(context);
        tabController.addListener(() {
          if (!tabController.indexIsChanging) {
            // Your code goes here.
            // To get index of current tab use tabController.index
          }
        });
        return Scaffold(
          appBar: AppBar(
            title: Text("'Hello world' messager"),
            bottom: TabBar(
              tabs: [Tab(text: "Login",), Tab(text : "Registration")]
            ),
          ),
          body: TabBarView(
            children: [LoginPage(onLogin), RegisterPage()],
          ),
        );
      }),
    );
  }
}

//------------------------------------------------------------------------------
class RegisterPage extends StatelessWidget {

  final _loginContorller = TextEditingController();
  final _passwordController = TextEditingController();
  final _repeatPasswordController  = TextEditingController();


  void _submit(BuildContext context)
  {

      if(_passwordController.text != _repeatPasswordController.text) {
          showMessage("Passwords is differrent", context);
          return;
      }

      final login = _loginContorller.text;
      final pass = _passwordController.text;

      if(login.isEmpty) {
        showMessage("Login is empty", context);
        return;
      }

      print("TODO SUBMIT $login, $pass");
      //TODO AFTER success registration

      APPClient().registration(
          login,
          pass,
              (result) {
                showMessage("Registration sccess! Go to login", context);
              },
              (message) {
                showMessage("Error:" + message, context);
              });
  }

  @override
  Widget build(BuildContext context) {
      return Column(
          children: [
            TextField(
              controller: _loginContorller,
              decoration: InputDecoration(hintText: "login"),),
            TextField(
                controller: _passwordController,
                obscureText: true,
                decoration: InputDecoration(hintText: "password")),
            TextField(
                controller: _repeatPasswordController,
                obscureText: true,
                decoration: InputDecoration(hintText: "repeat password")),
            RaisedButton(
              child: Text("Check and Go"),
              onPressed: (){_submit(context);},)
          ],
      );
  }
}

//------------------------------------------------------------------------------
class LoginPage extends StatelessWidget {

  final _loginContorller = TextEditingController();
  final _passwordController = TextEditingController();

  final void Function(UserData userData) onLogin;

  LoginPage(
    this.onLogin
  );

  void _submit(BuildContext context)
  {
    final login = _loginContorller.text;
    final pass = _passwordController.text;

    if(login.isEmpty) {
      showMessage("Login is empty", context);
      return;
    }

    print("TODO SUBMIT $login, $pass");

    APPClient().login(
        login,
        pass,
            (result) {
                  onLogin(UserData(result["username"], result["token"]));
            },
            (message) {
                  showMessage(message, context);
            });
  }


  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TextField(
          controller: _loginContorller,
          decoration: InputDecoration(hintText: "login"),),
        TextField(
            controller: _passwordController,
            obscureText: true,
            decoration: InputDecoration(hintText: "password")),

        RaisedButton(
          child: Text("Check and Go"),
          onPressed: (){_submit(context);},)
      ],
    );
  }
}