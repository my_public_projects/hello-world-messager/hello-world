import 'dart:collection';
import 'dart:typed_data';
import 'dart:io';
import 'dart:convert';
import 'dart:async';

import 'package:flutter_app/chatData.dart';

class Request {
  final void Function(Map<String, dynamic> result) onResult;
  final void Function(String message) onFail;
  Request(this.onResult, this.onFail);
}

class APPClient {

// public:

  factory APPClient()
  {
    return _instance;
  }



//API METHODS

  void registration(
      String userName,
      String password,
      void Function(Map<String, dynamic> result) onResult,
      void Function(String message) onFail)
  {
      final id = _getId();
      String req = jsonEncode({
        "jsonrpc" : "2.0",
        "method" : "registration",
        "params" : {
          "username" : userName,
          "password" : password,
        },
        "id" : id
      });

      _socket.write(req);

      _requests[id] = Request(onResult, onFail);
  }

  void login(
      String userName,
      String password,
      void Function(Map<String, dynamic> result) onResult,
      void Function(String message) onFail)
  {
    final id = _getId();

    String req = jsonEncode({
        "jsonrpc" : "2.0",
        "method" : "login",
        "params" : {
          "username" : userName,
          "password" : password,
        },
        "id" : id
    });

    _socket.write(req);

    _requests[id] = Request(onResult, onFail);

  }

  void getChatList(
      String token,
      void Function(Map<String, dynamic> result) onResult,
      void Function(String message) onFail)
  {
    final id = _getId();
    String req = jsonEncode({
      "jsonrpc" : "2.0",
      "method" : "chatlist",
      "params" : {
          "token" : token
      },
      "id" : id
    });

    _socket.write(req);

    _requests[id] = Request(onResult, onFail);

  }

  void createChat(
      String token,
      String name,
      void Function(Map<String, dynamic> result) onResult,
      void Function(String message) onFail)
  {
    final id = _getId();
    String req = jsonEncode({
      "jsonrpc" : "2.0",
      "method" : "createchat",
      "params" : {
        "token" : token,
        "name" : name
      },
      "id" : id
    });

    _socket.write(req);

    _requests[id] = Request(onResult, onFail);
  }

  void getChat(
      String token,
      String chatId,
      void Function(Map<String, dynamic> result) onResult,
      void Function(String message) onFail)
  {
    final id = _getId();
    String req = jsonEncode({
      "jsonrpc" : "2.0",
      "method" : "getchat",
      "params" : {
        "token" : token,
        "id" : chatId
      },
      "id" : id
    });

    _socket.write(req);

    _requests[id] = Request(onResult, onFail);
  }

  void sendMessage(
      String token,
      String chatId,
      Message message,
      void Function(Map<String, dynamic> result) onResult,
      void Function(String message) onFail)
  {
    final id = _getId();
    String req = jsonEncode({
      "jsonrpc" : "2.0",
      "method" : "sendmessage",
      "params" : {
        "token" : token,
        "chatid" : chatId,
        "message" : {
          "owner" : message.owner,
          "time" : message.time,
          "data" : message.data
        }
      },
      "id" : id
    });

    _socket.write(req);

    _requests[id] = Request(onResult, onFail);
  }

  void addUserToChat(
      String token,
      String chatId,
      String userName,
      void Function(Map<String, dynamic> result) onResult,
      void Function(String message) onFail)
  {
    final id = _getId();
    String req = jsonEncode({
      "jsonrpc" : "2.0",
      "method" : "addusertochat",
      "params" : {
        "token" : token,
        "chatid" : chatId,
        "username" : userName
      },
      "id" : id
    });

    _socket.write(req);

    _requests[id] = Request(onResult, onFail);
  }

  void getUpdates(
      String token,
      void Function(Map<String, dynamic> result) onResult,
      void Function(String message) onFail)
  {
    final id = _getId();
    String req = jsonEncode({
      "jsonrpc" : "2.0",
      "method" : "getupdates",
      "params" : {
        "token" : token
      },
      "id" : id
    });

    _socket.write(req);

    _requests[id] = Request(onResult, onFail);

  }

  //other

  void connect(String server, int port)
  {
      _serverAddress = server;
      _serverPort = port;
      _reconnect();
  }

  bool isConnected()
  {
      return _connected;
  }

  String server()
  {
      return _serverAddress;
  }

  int port()
  {
      return _serverPort;
  }

//private methods
  APPClient._privateConstructor()
  {
    _serverAddress = "192.168.1.37";
    _serverPort = 8080;
    _reconnect();
  }

  void _reconnect()
  {
    _connected = false;
    Socket.connect(_serverAddress, _serverPort)
        .then(_onConnect)
        .catchError(_onError);
  }

  void _send(String data)
  {
      if(!_connected) {
        _sendQueue.add(data);
        return;
      }

      _socket.write(data);
  }

  void _sender()
  {
    if(_sendQueue.isEmpty || !_connected) return;

    _send(_sendQueue.first);
    _sendQueue.removeFirst();

    Timer(Duration(milliseconds: 500), _sender);

  }

  void _onConnect(Socket sock)
  {
    print("[APPClient]: Connected");
    _socket = sock;
    _socket.listen(_onMessage, onError: _onError, onDone: _onDone);
    _connected = true;

    _sender();

  }

  void _handleResponse(Map<String, dynamic> resp)
  {
      String id = resp["id"];
      assert(id != null);
      if(!_requests.containsKey(id)) {
          print("[APPClient]: wrong response id: " + id);
          return;
      }

      Request req = _requests[id];

      if(resp.containsKey("error")) {
        Map<String, dynamic> error = resp["error"];
        req.onFail(error["message"]);
        _requests.remove(id);
        return;
      }

      if(resp.containsKey("result")) {
        Map<String, dynamic> result = resp["result"];
        req.onResult(result);
        return;
      }

      print("[APPClient]: BAD RESPONSE FORMAT");

  }

  void _handlePing(String data)
  {
      _socket.write(data);
      print("[APPClient]: Handle ping!");
  }

  void _onMessage(Uint8List data) {
    final str = Utf8Decoder().convert(data);
    print("[RECEIVE]:" + str);
    Map<String, dynamic> message = jsonDecode(str);
    if(message.isEmpty) {
      print("[APPClient]: bad data received" + str);
    }

    if(message.containsKey("method")) {
        _handlePing(str);
        return;
    }

    _handleResponse(message);
  }

  void _onError(error) {
    _connected = false;
    print("[APPClient]:" + error.toString());
    Timer(Duration(milliseconds: 1000), (){
      print("[APPClient]: try reconnect"); _reconnect();});
  }

  void _onDone() {
    _connected = false;
    print("[APPClient]: Connection close");
    Timer(Duration(milliseconds: 1000), (){
      print("[APPClient]: try reconnect"); _reconnect();});
  }

  String _getId()
  {
      ++_lastId;
      return _lastId.toString();
  }

  //private fields
  String _serverAddress;
  int _serverPort;
  Socket _socket;
  bool _connected = false;
  int _lastId = 0;

  final Map<String, Request> _requests = Map<String, Request>();

  final Queue<String> _sendQueue = Queue<String>();

  static final APPClient _instance = APPClient._privateConstructor();


}


