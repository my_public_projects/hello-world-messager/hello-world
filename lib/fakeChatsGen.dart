import 'chatData.dart';
import 'dart:math';

//------------------------------------------------------------------------------
class FakeChatsGen {

  final String _usrName;

  FakeChatsGen(this._usrName) : assert(_usrName != null);

  String _getRandomString(int size)
  {
    const _chars = '   AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz ';
    String res = "";
    final rnd = Random();
    //СЛОЖНА!!!!
    res = String.fromCharCodes(Iterable.generate(size, (int index){
      return _chars.codeUnitAt(rnd.nextInt(_chars.length));
    }));

    return res;
  }

  ChatData _makeFakeChat()
  {

    ChatData fake = ChatData();
    fake.chatName = _getRandomString(10);

    final rnd = Random();

    final count = 10 + rnd.nextInt(50);
    for(int i = 0; i < count; ++i) {
      fake.messages.add(
          Message( i.toString() + _getRandomString(2 + rnd.nextInt(100)),
              rnd.nextInt(20).isOdd ? fake.chatName : _usrName));
    }

    return fake;

  }

  List<ChatData> fakeChats(int N)
  {
    List<ChatData> res = [];
    for(int i = 0; i < N; ++i)
    {
      res.add(_makeFakeChat());
    }

    return res;

  }

}//FakeChatGen