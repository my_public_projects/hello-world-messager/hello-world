import 'package:flutter/material.dart';
import 'dart:async';

import 'chatData.dart';
import 'userData.dart';
import 'app_client.dart';

//------------------------------------------------------------------------------
class ChatWidget extends StatefulWidget {


  final ChatData _data;
  final UserData _userData;

  ChatWidget(
      this._data,
      this._userData
      ) : assert (_data != null),
        assert(_userData != null);

  @override
  _ChatState createState() => _ChatState(_data, _userData);
}//ChatWidget

//------------------------------------------------------------------------------
class _ChatState extends State<ChatWidget> {
  final ChatData _data;
  final UserData _userData;

  final _scrollController = ScrollController();

  final _myMessBGColor = Colors.black26;
  final _theirsMessBGColor = Colors.black12;
  final _myMessColor = Colors.black;
  final _theirsMessColor = Colors.black;

  _ChatState(
      this._data,
      this._userData
      ) : assert(_data != null),
        assert(_userData != null)
  {
      _checkUpdates();
  }

  void _checkUpdates()
  {
      Timer(Duration(seconds: 1), _checkUpdates);
      if(_data.newMessages <= 0)
        return;
      _data.newMessages = 0;
      APPClient().getChat(
          _userData.token,
          _data.chatId,
              (result) {
                List<dynamic> rawMessages = result["messages"];

                List<Message> messages = rawMessages.map((e) {
                  Map<String, dynamic> jmessage = e;

                  Message message = Message(jmessage["data"], jmessage["owner"]);
                  message.time = jmessage["time"];

                  return message;

                }).toList();

                //chat.messages = messages;
                setState(() {
                    _data.messages = messages;
                });
              },
              (message) { }
              );

  }

  Widget _buildMess(Message msg, bool right)
  {

    final rowChildren = List<Widget>();

    final mess =
    Container(
      alignment: right ? Alignment.centerRight : Alignment.centerLeft,
      decoration: BoxDecoration(
        color: right ? _myMessBGColor : _theirsMessBGColor,
        borderRadius: BorderRadius.circular(5.0),
      ),
      child: Column(children : [
        Container(// MESSAGE TIME
          height: 16.0,
          margin: right ? EdgeInsets.only(left: 10.0) : EdgeInsets.only(right: 10.0),
          alignment: right ?  Alignment.centerLeft :  Alignment.centerRight,
          child: Text(msg.owner + " at " + msg.time.toString(), style: TextStyle(fontSize: 7),),
        ),
        Text(//MESSAGE DATA
          msg.data,
          style: TextStyle(
              color: right ? _myMessColor : _theirsMessColor,
              fontSize: 14.0),
        )
      ]),
    );

    double shiftSize = 0;
    final size = msg.data.length;
    if(size > 40) {
      shiftSize = 64;
    } else if(size > 20) {
      shiftSize = 96;
    } else {
      shiftSize = 146;
    }

    final shifter = Container(width: shiftSize);

    if(right) {
      rowChildren.add(shifter);
      rowChildren.add(Expanded(child: mess));
    } else {
      rowChildren.add(Expanded(child: mess));
      rowChildren.add(shifter);
    }
    return ListTile(
        title: Row(
            mainAxisAlignment: right ? MainAxisAlignment.end : MainAxisAlignment.start,
            children: rowChildren));
  }

  Widget _buildMessages(List<Message> messages)
  {
    final messageList = messages.map((Message msg){
      if(msg.owner == _userData.userName)
      {
        return _buildMess(msg, true);
      } else
      {
        return _buildMess(msg, false);
      }
    });
    return ListView(children: messageList.toList(), controller: _scrollController,);
  }

  void _sendMessage(Message message, void Function() onSend)
  {
    APPClient().sendMessage(
        _userData.token,
        _data.chatId,
        message,
            (result) {
              setState(() {
                _data.messages.add(message);
              });
              onSend();
            },
            (message) {
              setState(() {
                _data.messages.add(Message("Error: " + message, "SERVER"));
              });
            }
            );
  }

  Widget _buildInput()
  {
    final controller = TextEditingController();
    final input = TextField(
      controller: controller,
      decoration: InputDecoration(hintText:"write message"),
    );

    final textExtractor = ()
    {
      _sendMessage(Message(controller.text, _userData.userName), (){
        controller.clear();
      });

    };

    final sendButton = IconButton(
      icon: Icon(Icons.send, color: Colors.black,),
      onPressed: textExtractor,
      tooltip: "Send",

    );

    return Row(children: [Expanded(child:input), sendButton],);
  }

  void _scrollDown()
  {
    _scrollController.animateTo(
      _scrollController.position.maxScrollExtent,
      duration: Duration(milliseconds: 500),
      curve: Curves.easeOut,
    );
  }

  void _sendAdd(BuildContext context, String userName)
  {
      APPClient().addUserToChat(
          _userData.token,
          _data.chatId,
          userName,
              (result) {
                  Navigator.of(context).pop();
                  _sendMessage(
                      Message(
                          _userData.userName +  " add " +
                          userName + " to chat", _userData.userName
                      ), (){}
                  );
              },
              (message) {
                  showDialog(
                    context: context,
                    builder: (BuildContext context)
                      {
                        return AlertDialog(
                          title: Text("ERROR: " + message),
                        );
                      }
                  );
              }
              );
  }

  void _onAddUser(BuildContext context)
  {

      final _textController = TextEditingController();

      showDialog(
        context: context,
        builder: (BuildContext context)
          {
              return AlertDialog(
                title: Text("Enter user login to add"),
                content: TextField(
                  controller: _textController ,
                  decoration: InputDecoration(hintText: "USERNAME"),
                ),
                actions: [
                  RaisedButton(
                    child: Text("ADD"),
                    onPressed: () { _sendAdd(context, _textController.text);}
                  )
                ],
              );
          }
      );
  }

  Widget build(BuildContext context)
  {

    final result =
    Scaffold(
      appBar : AppBar(
        title:Text("Chat: " + _data.chatName),
        actions: [
          IconButton(
            icon: Icon(Icons.add),
            onPressed: (){_onAddUser(context);},
          )
        ],
      ),
      body: Column(
        children: [
          Expanded(child : _buildMessages(_data.messages),),
          Container(child: _buildInput(), height: 64,)
        ],
      ),
    );


    Timer(Duration(milliseconds: 100), _scrollDown);

    return result;

  }
}//_ChatState